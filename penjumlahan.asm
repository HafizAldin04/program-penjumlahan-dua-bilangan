

;NAMA : HAFIZ ALDIN
;NIM  : 2100018071

                        

org 100h

jmp mulai       ; Melompati pesan

msg :    db      "1-TAMBAH",0dh,0ah,"2-KALI",0dh,0ah,"3-KURANG",0dh,0ah,"4-BAGI", 0Dh,0Ah, '$'
msg2:    db      0dh,0ah,"Masukan Angka Pertama : $"
msg3:    db      0dh,0ah,"Masukan Angka Kedua: $"
msg4:    db      0dh,0ah,"Error! $" 
msg5:    db      0dh,0ah,"Hasil : $" 
msg6:    db      0dh,0ah ,'Terimakasih, Keluar dan coba lagi...', 0Dh,0Ah, '$'

mulai:  mov ah,9
        mov dx, offset msg ;Menampilkan pesan yang pertama dengan int 21h
        int 21h
        mov ah,0                       
        int 16h  ;Kita akan menggunakan int 16h untuk pemilihan aritmatika
        cmp al,31h ;Pemilihan akan disimpan di al
        je Tambah
        cmp al,32h
        je Kali
        cmp al,33h
        je Kurang
        cmp al,34h
        je Bagi
        mov ah,09h
        mov dx, offset msg4 ;Akan menampilkan error jika salah memilih
        int 21h
        mov ah,0
        int 16h
        jmp mulai
        
Tambah:   mov ah,09h
            mov dx, offset msg2  ;Menampilkan pesan yang kedua dengan int 21h
            int 21h
            mov cx,0 ;Pemanggilan TambahNo
            call TambahNo  ;Untuk membuat angka lebih dari 1 digit
            push dx
            mov ah,9
            mov dx, offset msg3  ;Menampilkan pesan yang ketiga dengan int 21h
            int 21h 
            mov cx,0
            call TambahNo
            pop bx
            add dx,bx
            push dx 
            mov ah,9
            mov dx, offset msg5  ;Menampilkan pesan yang kelima dengan int 21h
            int 21h
            mov cx,10000
            pop dx
            call Lihat
            jmp Keluar
            
TambahNo:    mov ah,0
            int 16h ;Menggunakan 16h untuk membaca inputan
            mov dx,0  
            mov bx,1 
            cmp al,0dh ;Pemilihan akan disimpan di al, tekan enter untuk selesai
            je FormNo ;Jika tidak, masuk ke form nomor
            sub ax,30h ;lebih dari 5 digit akan masuk desimal
            call LihatNo ;Melihat angka
            mov ah,0 
            push ax  
            inc cx   
            jmp TambahNo ;Kembali ke TambahNo setelah enter
   

;Pengambilan nomor secara terpisan dan menyimpan dalam 1 bit
FormNo:     pop ax  
            push dx      
            mul bx
            pop dx
            add dx,ax
            mov ax,bx       
            mov bx,10
            push dx
            mul bx
            pop dx
            mov bx,ax
            dec cx
            cmp cx,0
            jne FormNo
            ret   


       
       
Lihat:  mov ax,dx
       mov dx,0
       div cx 
       call LihatNo
       mov bx,dx 
       mov dx,0
       mov ax,cx 
       mov cx,10
       div cx
       mov dx,bx 
       mov cx,ax
       cmp ax,0
       jne Lihat
       ret


LihatNo:    push ax 
           push dx 
           mov dx,ax 
           add dl,30h
           mov ah,2
           int 21h
           pop dx  
           pop ax
           ret
      
   
Keluar:   mov dx,offset msg6;Menampilkan pesan yang keenam dengan
        mov ah, 09h
        int 21h  


        mov ah, 0
        int 16h

        ret
            
                       
Kali:   mov ah,09h
            mov dx, offset msg2
            int 21h
            mov cx,0
            call TambahNo
            push dx
            mov ah,9
            mov dx, offset msg3
            int 21h 
            mov cx,0
            call TambahNo
            pop bx
            mov ax,dx
            mul bx 
            mov dx,ax
            push dx 
            mov ah,9
            mov dx, offset msg5
            int 21h
            mov cx,10000
            pop dx
            call Lihat 
            jmp Keluar 


Kurang:   mov ah,09h
            mov dx, offset msg2
            int 21h
            mov cx,0
            call TambahNo
            push dx
            mov ah,9
            mov dx, offset msg3
            int 21h 
            mov cx,0
            call TambahNo
            pop bx
            sub bx,dx
            mov dx,bx
            push dx 
            mov ah,9
            mov dx, offset msg5
            int 21h
            mov cx,10000
            pop dx
            call Lihat 
            jmp Keluar 
    
            
Bagi:     mov ah,09h
            mov dx, offset msg2
            int 21h
            mov cx,0
            call TambahNo
            push dx
            mov ah,9
            mov dx, offset msg3
            int 21h 
            mov cx,0
            call TambahNo
            pop bx
            mov ax,bx
            mov cx,dx
            mov dx,0
            mov bx,0
            div cx
            mov bx,dx
            mov dx,ax
            push bx 
            push dx 
            mov ah,9
            mov dx, offset msg5
            int 21h
            mov cx,10000
            pop dx
            call Lihat
            pop bx
            cmp bx,0
            je Keluar 
            jmp Keluar


